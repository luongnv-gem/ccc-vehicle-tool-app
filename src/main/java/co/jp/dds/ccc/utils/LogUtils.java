package co.jp.dds.ccc.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LogUtils {
	private static final Logger LOGGER = LoggerFactory.getLogger(LogUtils.class);

	private static final String ERROR_TEMP = "%s";

	public static void error(Logger logger, String message) {
		logger.error(String.format(ERROR_TEMP, message));
	}

	public static void info(Logger logger, String message) {
		logger.info(message);
	}

	public static void error(Logger logger, Exception e) {
		if (logger.isDebugEnabled()) {
			e.printStackTrace();
		}

		logger.error(String.format(ERROR_TEMP, e.getMessage()));
	}


}
