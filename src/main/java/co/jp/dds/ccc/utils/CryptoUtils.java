package co.jp.dds.ccc.utils;

import org.bouncycastle.crypto.Digest;
import org.bouncycastle.crypto.prng.DigestRandomGenerator;
import org.bouncycastle.crypto.util.DigestFactory;
import org.bouncycastle.jcajce.util.BCJcaJceHelper;
import org.bouncycastle.jcajce.util.JcaJceHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.SecureRandom;
import java.util.Arrays;

public class CryptoUtils {
	public final static Digest SHA_1 = DigestFactory.createSHA1();

	private static final Logger LOGGER = LoggerFactory.getLogger(CryptoUtils.class);

	public static byte[] generateIdentifier(Digest digest, int len) {
		if (len > digest.getDigestSize()) {
			LOGGER.error("Please enter size less than " + digest.getDigestSize());
			return null;
		}
		SecureRandom secureRandom = new SecureRandom();
		byte[] seed = secureRandom.generateSeed(8);
		new DigestFactory();
		DigestRandomGenerator rGen = new DigestRandomGenerator(digest);
		byte[] output = new byte[digest.getDigestSize()];
		rGen.addSeedMaterial(seed);
		rGen.nextBytes(output);
		return Arrays.copyOfRange(output, 0, len);

	}

	public static byte[] generateVehicleIdentifier() {
		return generateIdentifier(SHA_1, 8);
	}

}
