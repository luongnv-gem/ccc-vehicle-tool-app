package co.jp.dds.ccc.utils;

import co.jp.dds.ccc.model.StorageFileURL;
import co.jp.dds.ccc.model.VehicleStorage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;

@Component
public class StorageUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(StorageUtils.class);
    public static final String STORAGE_FILE_NAME = "vehicle_storage.ccc";
    private static final String STORAGE_FILE_URL = StorageFileURL.getInstance();

    public VehicleStorage fetch() {
        return read();
    }

    public void update(VehicleStorage vehicleStorage) {
        if(vehicleStorage == null) {
            return;
        }
        write(vehicleStorage);
    }

    private VehicleStorage read() {
        Path filePath = Paths.get(STORAGE_FILE_URL, STORAGE_FILE_NAME);
        InputStream is = null;
        VehicleStorage vehicleStorage;
        ObjectInputStream ois = null;
        try {
            File storageFile = new File(filePath.toString());
            is = new FileInputStream(storageFile);
            ois = new ObjectInputStream(is);
            vehicleStorage = (VehicleStorage) ois.readObject();
        } catch (Exception e) {
            LogUtils.error(LOGGER, e);
            vehicleStorage = new VehicleStorage();
        } finally {

            try {
                if (ois != null) {
                    ois.close();
                }
                if (is != null) {
                    is.close();
                }
            } catch (IOException e) {
                LogUtils.error(LOGGER, e);
            }

        }
        return vehicleStorage;
    }

    private void write(VehicleStorage vehicleStorage) {
        if (vehicleStorage == null) {
            return;
        }
        Path filePath = Paths.get(STORAGE_FILE_URL, STORAGE_FILE_NAME);
        try {
            File storageFile = new File(filePath.toString());
            File parent = storageFile.getParentFile();
            if(!parent.exists()) {
                parent.mkdir();
            }
            FileOutputStream fileOut = new FileOutputStream(storageFile);
            ObjectOutputStream objectOut = new ObjectOutputStream(fileOut);
            objectOut.writeObject(vehicleStorage);
            objectOut.flush();
            objectOut.close();
            fileOut.close();
        } catch (IOException e) {
            LogUtils.error(LOGGER, e);
        }
    }
}
