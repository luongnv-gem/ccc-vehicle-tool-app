package co.jp.dds.ccc.utils;

import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.pkcs.PrivateKeyInfo;
import org.bouncycastle.asn1.sec.ECPrivateKey;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.x9.X9ObjectIdentifiers;
import org.bouncycastle.jce.interfaces.ECPublicKey;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.util.io.pem.PemObject;
import org.bouncycastle.util.io.pem.PemReader;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.logging.Logger;

public class PemFileUtils {

	protected final static Logger LOGGER = Logger.getLogger(String.valueOf(PemFileUtils.class));

	public static PemObject getPemObjectFromFile(File file) throws IOException {
		PemObject pemObject;
		try (PemReader pemReader = new PemReader(new InputStreamReader(new FileInputStream(file)))) {
			pemObject = pemReader.readPemObject();
		}
		return pemObject;
	}

	public static KeyPair generateKeyPair(KeyFactory factory, File privateKeyFile, File publicKeyFile)
			throws Exception {
		if (factory == null) {
			factory = KeyFactory.getInstance("EC", "BC");
		}
		PrivateKey privateKey = generateECPrivateKey(privateKeyFile);
		PublicKey publicKey = generatePublicKey(factory, publicKeyFile);
		return new KeyPair(publicKey, privateKey);
	}

	public static PrivateKey generateECPrivateKey(File filename) throws Exception {
		byte[] content = PemFileUtils.getPemObjectFromFile(filename).getContent();
		ASN1Sequence seq = ASN1Sequence.getInstance(content);
		ECPrivateKey pKey = ECPrivateKey.getInstance(seq);
		AlgorithmIdentifier algId = new AlgorithmIdentifier(X9ObjectIdentifiers.id_ecPublicKey, pKey.getParameters());
		byte[] server_pkcs8 = new PrivateKeyInfo(algId, pKey).getEncoded();
		KeyFactory fact = KeyFactory.getInstance("EC", new BouncyCastleProvider());
		return fact.generatePrivate(new PKCS8EncodedKeySpec(server_pkcs8));
	}

	public static PrivateKey generateECPrivateKey(byte[] content) throws Exception {
		// byte[] content = PemFileUtils.getPemObjectFromFile(is).getContent();
		ASN1Sequence seq = ASN1Sequence.getInstance(content);
		ECPrivateKey pKey = ECPrivateKey.getInstance(seq);
		AlgorithmIdentifier algId = new AlgorithmIdentifier(X9ObjectIdentifiers.id_ecPublicKey, pKey.getParameters());
		byte[] server_pkcs8 = new PrivateKeyInfo(algId, pKey).getEncoded();
		KeyFactory fact = KeyFactory.getInstance("EC", new BouncyCastleProvider());
		return fact.generatePrivate(new PKCS8EncodedKeySpec(server_pkcs8));
	}

	public static PublicKey generatePublicKey(KeyFactory factory, File filename)
			throws InvalidKeySpecException, IOException {
		byte[] content = PemFileUtils.getPemObjectFromFile(filename).getContent();
		X509EncodedKeySpec pubKeySpec = new X509EncodedKeySpec(content);
		return factory.generatePublic(pubKeySpec);
	}

	public static X509Certificate loadX509CertFromFile(File file) throws Exception {
		CertificateFactory fact = CertificateFactory.getInstance("X.509");
		FileInputStream is = new FileInputStream(file);
		return (X509Certificate) fact.generateCertificate(is);
	}

	public static PublicKey generatePublicKey(File filename) throws Exception {
		byte[] content = PemFileUtils.getPemObjectFromFile(filename).getContent();
		X509EncodedKeySpec pubKeySpec = new X509EncodedKeySpec(content);
		KeyFactory factory = KeyFactory.getInstance("EC", new BouncyCastleProvider());
		return factory.generatePublic(pubKeySpec);
	}

	public static PublicKey generatePublicKey(byte[] content) throws Exception {
		// byte[] content = PemFileUtils.getPemObjectFromFile(is).getContent();
		X509EncodedKeySpec pubKeySpec = new X509EncodedKeySpec(content);
		KeyFactory factory = KeyFactory.getInstance("EC", new BouncyCastleProvider());
		return factory.generatePublic(pubKeySpec);
	}

	public static byte[] convertPublicKeyToEcPoint(PublicKey publicKey) {
		try {
			byte[] byte_pubkey = publicKey.getEncoded();
			return convertPublicKeyToEcPoint(byte_pubkey);
		} catch (Exception ex) {
			System.err.format("Exception: %s%n", ex);
		}
		return new byte[0];
	}

	public static byte[] convertPublicKeyToEcPoint(byte[] publicKeyData) {
		try {
			KeyFactory factory = KeyFactory.getInstance("ECDH", new BouncyCastleProvider());
			ECPublicKey ecPublicKey = (ECPublicKey) factory.generatePublic(new X509EncodedKeySpec(publicKeyData));
			return ecPublicKey.getQ().getEncoded(false);
		} catch (Exception ex) {
			System.err.format("Exception: %s%n", ex);
		}
		return new byte[0];
	}

}
