package co.jp.dds.ccc.model;

import co.jp.dds.ccc.utils.LogUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ImmobilizerToken implements Serializable {

	private static final long serialVersionUID = 1L;

	private byte[] ownerToken;
	private List<byte[]> friendTokens = new ArrayList<byte[]>();

	public byte[] getOwnerToken() {
		return ownerToken;
	}

	public List<byte[]> getFriendTokens() {
		return friendTokens;
	}

	public void clear() {
		ownerToken = null;
		friendTokens.clear();
	}
}
