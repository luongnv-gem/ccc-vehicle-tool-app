package co.jp.dds.ccc.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AttestationPackage {

    private static Logger LOGGER = LoggerFactory.getLogger(AttestationPackage.class);

    private byte[] attPackage;

    private byte[] certVersion;

    private byte[] random;

    private byte[] pkAtt;

    private byte[] entitlementData;

    private byte[] keyTracking;

    private byte[] ktsSignature;

    public AttestationPackage(byte[] attPackage, byte[] certVersion,
                              byte[] random, byte[] pkAtt,
                              byte[] entitlementData, byte[] keyTracking,
                              byte[] ktsSignature) {
        this.attPackage = attPackage;
        this.certVersion = certVersion;
        this.random = random;
        this.pkAtt = pkAtt;
        this.entitlementData = entitlementData;
        this.keyTracking = keyTracking;
        this.ktsSignature = ktsSignature;
    }

    public byte[] getAttPackage() {
        return attPackage;
    }

    public void setAttPackage(byte[] attPackage) {
        this.attPackage = attPackage;
    }

    public byte[] getCertVersion() {
        return certVersion;
    }

    public void setCertVersion(byte[] certVersion) {
        this.certVersion = certVersion;
    }

    public byte[] getEntitlementData() {
        return entitlementData;
    }

    public void setEntitlementData(byte[] entitlementData) {
        this.entitlementData = entitlementData;
    }

    public byte[] getKeyTracking() {
        return keyTracking;
    }

    public void setKeyTracking(byte[] keyTracking) {
        this.keyTracking = keyTracking;
    }

    public byte[] getRandom() {
        return random;
    }

    public void setRandom(byte[] random) {
        this.random = random;
    }

    public byte[] getPkAtt() {
        return pkAtt;
    }

    public void setPkAtt(byte[] pkAtt) {
        this.pkAtt = pkAtt;
    }

    public byte[] getKtsSignature() {
        return ktsSignature;
    }

    public void setKtsSignature(byte[] ktsSignature) {
        this.ktsSignature = ktsSignature;
    }
}
