package co.jp.dds.ccc.model;

import org.bouncycastle.asn1.ASN1Enumerated;

import java.io.Serializable;

public class StandardProfiles extends ASN1Enumerated implements Serializable {

    public static final int full = 0;
    public static final int accessOnly = 1;
    public static final int accessAndDriveRestricted = 2;
    public static final int carDelivery = 3;
    public static final int valet = 4;
    public static final int vehicleService = 5;

    public static StandardProfiles createFullProfile() {
        return StandardProfiles.create(full);
    }

    public static StandardProfiles createAccessOnlyProfile() {
        return StandardProfiles.create(accessOnly);
    }

    public static StandardProfiles createAccessAndDriveRestrictedProfile() {
        return StandardProfiles.create(accessAndDriveRestricted);
    }

    public static StandardProfiles createCarDeliveryProfile() {
        return StandardProfiles.create(carDelivery);
    }

    public static StandardProfiles createValetProfile() {
        return StandardProfiles.create(valet);
    }

    public static StandardProfiles createVehicleServiceProfile() {
        return StandardProfiles.create(vehicleService);
    }

    public static StandardProfiles create(int value) {
        switch (value) {
            case full:
            case accessOnly:
            case accessAndDriveRestricted:
            case carDelivery:
            case valet:
            case vehicleService:
                return new StandardProfiles(value);
            default:
                return null;
        }
    }

    private StandardProfiles(int value) {
        super(value);
    }
}
