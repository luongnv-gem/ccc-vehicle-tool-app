package co.jp.dds.ccc.model;

import org.bouncycastle.asn1.*;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.Serializable;

public class AccessProfiles extends ASN1Object implements ASN1Choice, Serializable {

    StandardProfiles standard;
    ASN1OctetString proprietary;

    public static AccessProfiles createStandardProfiles(StandardProfiles profiles) {
        return new AccessProfiles(profiles);
    }

    public static AccessProfiles createProprietaryProfiles(byte[] proprietary) {
        return new AccessProfiles(proprietary);
    }

    public boolean isStandard() {
        return standard != null;
    }

    public boolean isProprietary() {
        return proprietary != null;
    }

    @Override
    public ASN1Primitive toASN1Primitive() {
        return isStandard() ? standard.toASN1Primitive() : proprietary.toASN1Primitive();
    }

    private AccessProfiles(StandardProfiles standard) {
        this.standard = standard;
    }

    private AccessProfiles(byte[] proprietary) {
        this.proprietary = new DEROctetString(proprietary);
    }
}
