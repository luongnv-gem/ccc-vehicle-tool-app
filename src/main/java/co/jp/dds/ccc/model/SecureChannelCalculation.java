package co.jp.dds.ccc.model;

import java.io.Serializable;

public class SecureChannelCalculation implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 251489714974500156L;

	private SecureChannelParameter secureChannelParameter;

	public void increasePaddedCounter() {
		secureChannelParameter.increasePaddedCounter();
	}

	public byte[] getMacChaining() {
		return secureChannelParameter.getMacChainingVal();
	}

	public byte[] getPaddedCounter() {
		return secureChannelParameter.getPaddedCounterVal();
	}

	public SecureChannelParameter getChannelParam() {
		return this.secureChannelParameter;
	}
}
