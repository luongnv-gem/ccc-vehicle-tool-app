package co.jp.dds.ccc.model;

import java.io.Serializable;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.ArrayList;
import java.util.List;

import co.jp.dds.ccc.utils.CryptoUtils;
import sun.security.ec.ECPrivateKeyImpl;
import sun.security.ec.ECPublicKeyImpl;

public final class VehicleStorage implements Serializable {
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	private byte[] vehicleIdentifier;

	private byte[] vehiclePK;
	private byte[] vehicleSK;
	private byte[] vehiclePublicKeyCert;
	private byte[] vehicleOEMCACert;
	private List<byte[]> authorizedPKs = new ArrayList<>();
	private DigitalKeyTable digitalKeyTable;
	private boolean isPaired;
	private ImmobilizerToken immobilizerToken;
	private PrivateMailbox privateMailbox;
	private boolean isLock;
	private byte[] kPersistent;

	private boolean isImmobileTokenRequired;

	private boolean isRFRunning = false;
	private boolean isForceStandard;

	public byte[] getVehiclePK() {
		return vehiclePK;
	}

	public void setVehiclePK(byte[] vehiclePK) {
		this.vehiclePK = vehiclePK;
	}

	public byte[] getVehicleSK() {
		return vehicleSK;
	}

	public void setVehicleSK(byte[] vehicleSK) {
		this.vehicleSK = vehicleSK;
	}

	public DigitalKeyTable getDigitalKeyTable() {
		if (digitalKeyTable == null) {
			digitalKeyTable = new DigitalKeyTable();
		}
		return digitalKeyTable;
	}

	public void setDigitalKeyTable(DigitalKeyTable digitalKeyTable) {
		this.digitalKeyTable = digitalKeyTable;
	}

	public boolean isPaired() {
		return isPaired;
	}

	public void setPaired(boolean isPaired) {
		this.isPaired = isPaired;
	}

	public List<byte[]> getAuthorizedPKs() {
		return authorizedPKs;
	}

	public void setAuthorizedPKs(List<byte[]> authorizedPKs) {
		this.authorizedPKs = authorizedPKs;
	}

	public byte[] getVehiclePublicKeyCert() {
		return vehiclePublicKeyCert;
	}

	public void setVehiclePublicKeyCert(byte[] vehiclePublicKeyCert) {
		this.vehiclePublicKeyCert = vehiclePublicKeyCert;
	}

	public byte[] getVehicleIdentifier() {
		return vehicleIdentifier;
	}

	public void setVehicleIdentifier(byte[] vehicleIdentifier) {
		this.vehicleIdentifier = vehicleIdentifier;
	}

	public PrivateKey getECPrivateKey() throws InvalidKeyException {
		return new ECPrivateKeyImpl(vehicleSK);
	}

	public PublicKey getECPublicKey() throws InvalidKeyException {
		return new ECPublicKeyImpl(vehiclePK);
	}

	public KeyPair getKeyPair() throws InvalidKeyException {
		return new KeyPair(this.getECPublicKey(), this.getECPrivateKey());
	}


	public byte[] getVehicleOEMCACert() {
		return vehicleOEMCACert;
	}

	public void setVehicleOEMCACert(byte[] vehicleOEMCACert) {
		this.vehicleOEMCACert = vehicleOEMCACert;
	}

	public ImmobilizerToken getImmobilizerToken() {
		if (immobilizerToken == null) {
			immobilizerToken = new ImmobilizerToken();
		}
		return immobilizerToken;
	}

	public void setImmobilizerToken(ImmobilizerToken immobilizerToken) {
		this.immobilizerToken = immobilizerToken;
	}

	public PrivateMailbox getPrivateMailbox() {
		if (privateMailbox == null) {
			privateMailbox = new PrivateMailbox();
		}
		return privateMailbox;
	}

	public void setPrivateMailbox(PrivateMailbox privateMailbox) {
		this.privateMailbox = privateMailbox;
	}

	public boolean isLock() {
		return isLock;
	}

	public void setLock(boolean lock) {
		isLock = lock;
	}

	public boolean isImmobileTokenRequired() {
		return isImmobileTokenRequired;
	}

	public void setImmobileTokenRequired(boolean immobileTokenRequired) {
		isImmobileTokenRequired = immobileTokenRequired;
	}

	public byte[] getkPersistent() {
		return kPersistent;
	}

	public void setkPersistent(byte[] kPersistent) {
		this.kPersistent = kPersistent;
	}


	public boolean isRFRunning() {
		return isRFRunning;
	}

	public void setRFRunning(boolean isRFRunning) {
		this.isRFRunning = isRFRunning;
	}

	public boolean isForceStandard() {
		return isForceStandard;
	}

	public void setForceStandard(boolean forceStandard) {
		isForceStandard = forceStandard;
	}

	public VehicleStorage() {
		this.vehicleIdentifier = CryptoUtils.generateVehicleIdentifier();
	}

}
