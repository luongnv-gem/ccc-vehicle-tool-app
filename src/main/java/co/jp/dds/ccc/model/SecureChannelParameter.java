package co.jp.dds.ccc.model;

import java.io.Serializable;

public class SecureChannelParameter implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7308464414635202619L;
	private static final int PADDED_COUNTER_VAL_LEN = 16;
	private static final int MAC_CHAINING_VAL_LEN = 16;
	
	private static final byte[] DEFAULT_PADDED_COUNTER_VAL = new byte[]{0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01};
	private static final byte[] DEFAULT_MAC_CHAINING_VAL = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
	
	private byte[] paddedCounterVal = new byte[PADDED_COUNTER_VAL_LEN];
	private byte[] macChainingVal = new byte[MAC_CHAINING_VAL_LEN];
	
	private byte[] kencKey = new byte[16];
	private byte[] kmacKey = new byte[16];
	private byte[] krmacKey = new byte[16];

	public byte[] getPaddedCounterVal() {
		return paddedCounterVal;
	}



	public void setPaddedCounterVal(byte[] paddedCounterVal) {
		this.paddedCounterVal = paddedCounterVal;
	}



	public byte[] getMacChainingVal() {
		return macChainingVal;
	}



	public void setMacChainingVal(byte[] macChainingVal) {
		this.macChainingVal = macChainingVal;
	}



	public byte[] getKencKey() {
		return kencKey;
	}



	public void setKencKey(byte[] kencKey) {
		this.kencKey = kencKey;
	}



	public byte[] getKmacKey() {
		return kmacKey;
	}



	public void setKmacKey(byte[] kmacKey) {
		this.kmacKey = kmacKey;
	}



	public byte[] getKrmacKey() {
		return krmacKey;
	}



	public void setKrmacKey(byte[] krmacKey) {
		this.krmacKey = krmacKey;
	}
	
	public void increasePaddedCounter() {
		if(this.paddedCounterVal == null) {
			System.arraycopy(DEFAULT_PADDED_COUNTER_VAL, 0, this.paddedCounterVal, 0, PADDED_COUNTER_VAL_LEN);
		}
		
		this.paddedCounterVal[PADDED_COUNTER_VAL_LEN - 1] = (byte) (this.paddedCounterVal[PADDED_COUNTER_VAL_LEN - 1] + 1); 
	}
}
