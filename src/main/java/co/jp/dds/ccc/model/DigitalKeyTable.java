package co.jp.dds.ccc.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class DigitalKeyTable implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private byte[] ownerPublicKey;

	private Map<String, byte[]> friendPublicKeys = new HashMap<>();
	
	public byte[] getOwnerPublicKey() {
		return ownerPublicKey;
	}

	public Map<String, byte[]> getFriendPublicKeys() {
		if(friendPublicKeys == null){
			friendPublicKeys = new HashMap<>();
		}
		return friendPublicKeys;
	}

}
