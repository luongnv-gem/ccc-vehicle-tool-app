package co.jp.dds.ccc.model;

import co.jp.dds.ccc.constant.VehicleConstants;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.List;
import java.util.stream.IntStream;

public class PrivateMailbox implements Serializable {

	private static final long serialVersionUID = 1L;
	public static final int KEY_ATT_SIZE = 256;

	// vehicle oem data store with len byte array
	public static int KEY_ATTESTATION_DATA_LEN = 256;

	private final int MAX_BIT_COUNTER = 8;
	private int SIGNAL_BIT_FOR_SLOT_INDENT = 7;
//	private int SIGNAL_BIT_FOR_VEHICLE_PROP_DAT = 5;
	
	// vehicle oem data store with len byte array
	private int VEHICLE_PROP_DAT_DATA_LEN = 256;
		
	private int SIGNAL_BIT_FOR_KEY_ATT = 4;
	
	private int SIGNAL_BITMAP_OFFSET = 0;
	private int SLOT_IDENTIFIER_BITMAP_OFFSET = 1;
	private int SLOT_IDENTIFIERS_OFFSET = 2;
	

	private BitSet signalingBmp = new BitSet(MAX_BIT_COUNTER);
	private BitSet slotIdentifierBmp = new BitSet(MAX_BIT_COUNTER);
	private BitSet keyAttOffset = new BitSet(KEY_ATT_SIZE);
	private List<byte[]> slotIdentList = new ArrayList<byte[]>();

	private byte[] keyAtt = new byte[256];

	public PrivateMailbox() {
	}

	public int getSignalingBitmapOffset() {
		return SIGNAL_BITMAP_OFFSET;
	}
	
	public int getSlotIdentifierBitmapOffset() {
		return SLOT_IDENTIFIER_BITMAP_OFFSET;
	}
	
	public int getSlotIdentifierListOffset() {
		return SLOT_IDENTIFIERS_OFFSET;
	}
	
	public int getVehiclePropDatOffset() {
		return getSlotIdentifierListOffset() + 8 * VehicleConstants.IMMOBILIZER_IDENTIFY_LEN;
	}
	
	public int getAttesttationPackageOffset() {
		return getVehiclePropDatOffset() + VEHICLE_PROP_DAT_DATA_LEN;
	}
	
	public int getMailboxEndOffset() {
		return getAttesttationPackageOffset() + KEY_ATTESTATION_DATA_LEN;
	}

	public byte[] getSlotIdentifier(int index) {
		if (index >= MAX_BIT_COUNTER || index < 0) {
			return null;
		}

		if (slotIdentifierBmp.get(index)) {
			return slotIdentList.get(index);
		}

		return null;
	}

	public int getSlotIdentListOffset(){
		int currentOffSet = slotIdentifierBmp.length() - 1;
		if(currentOffSet > 7){
			return -1;
		}

		return currentOffSet;
	}

	public byte getSignalingValue() {
		final StringBuilder buffer = new StringBuilder();
		IntStream.range(0, MAX_BIT_COUNTER).mapToObj(i -> signalingBmp.get(i) ? '1' : '0').forEach(buffer::append);
		return (byte) Integer.parseInt(buffer.toString(), 2);
	}

	public byte getSlotIdentBmpValue() {
		final StringBuilder buffer = new StringBuilder();
		IntStream.range(0, MAX_BIT_COUNTER).mapToObj(i -> slotIdentifierBmp.get(i) ? '1' : '0').forEach(buffer::append);
		return (byte) Integer.parseInt(buffer.toString(), 2);
	}

	public BitSet getSignalingBmp() {
		return signalingBmp;
	}

	public int getKeyAttLen() {
		return keyAttOffset.length();
	}

	public List<byte[]> getSlotIdentList() {
		return slotIdentList;
	}

	public void clearAllSlotIdentifier(){
		signalingBmp = new BitSet(MAX_BIT_COUNTER);
		slotIdentifierBmp = new BitSet(MAX_BIT_COUNTER);
		slotIdentList = new ArrayList<>();
	}
}
