package co.jp.dds.ccc.model;

import java.util.Scanner;

public class StorageFileURL {

    private static final String INSTANCE;

    private StorageFileURL() {
    }

    static {
        System.out.println("****INPUT STORAGE FILE URL******");
        Scanner scanner = new Scanner(System.in);
        INSTANCE = scanner.nextLine();
        System.out.println("Working on " + StorageFileURL.getInstance());
    }

    public static String getInstance() {
        return INSTANCE;
    }

}
