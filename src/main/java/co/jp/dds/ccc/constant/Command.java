package co.jp.dds.ccc.constant;

import java.util.Arrays;

public enum Command {
    COMMUNICATION_MODE(1),
    TRANSACTION_MODE(2),
    GEN_STORAGE_FILE(3),
    Quit(4);

    private int value;

    Command(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public static Command from(int value) {
        return Arrays.stream(values())
                .filter(element -> element.value == value)
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("The argument "
                        + value + " doesn't match any Command"));
    }
}
