package co.jp.dds.ccc.constant;

import java.util.Arrays;

public enum VehicleMode {
    PAIRING("a"),
    LOCK_UNLOCK("b");

    private String key;

    VehicleMode(String key) {
        this.key = key;
    }

    public static VehicleMode from(String key) {
        return Arrays.stream(values())
                .filter(element -> element.key.equalsIgnoreCase(key))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("The argument "
                        + key + " doesn't match any VehicleMode"));
    }
}
