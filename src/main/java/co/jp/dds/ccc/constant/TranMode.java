package co.jp.dds.ccc.constant;

import java.util.Arrays;

public enum TranMode {

    STANDARD("a"),
    FAST("b");

    private String key;

    TranMode(String key) {
        this.key = key;
    }

    public static TranMode from(String key) {
        return Arrays.stream(values())
                .filter(element -> element.key.equalsIgnoreCase(key))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("The argument "
                        + key + " doesn't match any TranMode"));
    }
}
