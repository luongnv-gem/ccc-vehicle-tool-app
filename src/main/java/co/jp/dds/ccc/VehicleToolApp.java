package co.jp.dds.ccc;

import co.jp.dds.ccc.command.mode.ModeConversionCommand;
import co.jp.dds.ccc.command.storage.GenStorageFileCommand;
import co.jp.dds.ccc.command.transaction.TransactionModeCommand;
import co.jp.dds.ccc.command.transaction.subcommand.Fast;
import co.jp.dds.ccc.command.transaction.subcommand.Standard;
import co.jp.dds.ccc.constant.Command;
import co.jp.dds.ccc.constant.TranMode;
import co.jp.dds.ccc.constant.VehicleConstants;
import co.jp.dds.ccc.constant.VehicleMode;
import co.jp.dds.ccc.model.ImmobilizerToken;
import co.jp.dds.ccc.model.StorageFileURL;
import co.jp.dds.ccc.model.VehicleStorage;
import co.jp.dds.ccc.utils.LogUtils;
import co.jp.dds.ccc.utils.StorageUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import picocli.CommandLine;

import java.io.Console;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

import static co.jp.dds.ccc.utils.StorageUtils.STORAGE_FILE_NAME;

@SpringBootApplication
public class VehicleToolApp implements CommandLineRunner {

    private Logger LOGGER = LoggerFactory.getLogger(VehicleToolApp.class);

    public static void main(String[] args) throws Exception {
        SpringApplication.run(VehicleToolApp.class, args);
    }

    @Autowired
    private StorageUtils storageUtils;

    private ModeConversionCommand modeCommand;

    private TransactionModeCommand tranModeCommand;

    private GenStorageFileCommand genStorageFileCommand;

    private Standard standard;

    private Fast fast;

    @Autowired
    public VehicleToolApp(ModeConversionCommand modeCommand,
                          TransactionModeCommand tranModeCommand,
                          GenStorageFileCommand genStorageFileCommand,
                          Fast fast,
                          Standard standard) {
        this.modeCommand = modeCommand;
        this.tranModeCommand = tranModeCommand;
        this.genStorageFileCommand = genStorageFileCommand;
        this.fast = fast;
        this.standard = standard;
    }

    @Override
    public void run(String... args) throws Exception {
        StorageFileURL.getInstance();
        menu();
    }

    private void menu() {
        try {
            Thread.sleep(1000);
            System.out.println("=================================");
            System.out.println("****************************");
            System.out.println("**** VEHICLE TOOL APP ****");
            System.out.println("****************************");
            System.out.println("=================================");
            System.out.println("*1. Switch Pairing Mode/ Lock-Unlock mode");
            System.out.println("*2. Switch Fast transaction / Standard Transaction mode");
            System.out.println("*3. Generate storage file");
            System.out.println("*4. Quit");
            System.out.println("=================================");

            Scanner scanner = new Scanner(System.in);
            int command = scanner.nextInt();

            switch (Command.from(command)) {
                case COMMUNICATION_MODE: {
                    this.convertCommunicationMode();
                    break;
                }
                case TRANSACTION_MODE: {
                    this.switchFastStandardTran();
                    break;
                }
                case GEN_STORAGE_FILE: {
                    Thread.sleep(1000);
                    this.generateStorageFile();
                    break;
                }
                default: {
                    System.exit(0);
                }
            }
        } catch (Exception e) {
            LogUtils.error(LOGGER, e);
            this.menu();
        }
    }

    private void convertCommunicationMode() {
        fileExits();
        try {
            Thread.sleep(1000);
            VehicleStorage vehicleStorage = storageUtils.fetch();
            System.out.println("=================================");
            System.out.println("*1. Switch Pairing Mode/ Lock-Unlock mode");
            System.out.println("=================================");
            String state = vehicleStorage.isPaired() ? "LOCK/UNLOCK" : "PAIRING";
            System.out.println("CURRENT STATE: " + state);
            if (vehicleStorage.isPaired()) {
                System.out.println("*a. Pairing");
            } else {
                System.out.println("*YOU CAN'T CHANGE THE STATE...");
            }
            System.out.println("*x. Back");
            System.out.println("=================================");
            Scanner scanner = new Scanner(System.in);
            String mode = scanner.nextLine();
            if (mode.equalsIgnoreCase("x")) {
                this.menu();
            }

            CommandLine.run(modeCommand, VehicleMode.from(mode).name());
            menu();
        } catch (Exception e) {
            LogUtils.error(LOGGER, e);
            this.convertCommunicationMode();
        }
    }

    private void switchFastStandardTran() {
        fileExits();
        try {
            Thread.sleep(1000);
            System.out.println("=================================");
            System.out.println("*2. Switch Fast transaction / Standard Transaction mode");
            System.out.println("=================================");
            String state = isForceStandardProp() ? "STANDARD" : "FAST";
            System.out.println("CURRENT STATE: " + state);
            System.out.println("*a. STANDARD");
            System.out.println("*b. FAST");
            System.out.println("*x. Back");
            System.out.println("=================================");
            Scanner scanner = new Scanner(System.in);
            String mode = scanner.nextLine();
            if (mode.equalsIgnoreCase("x")) {
                this.menu();
            }

            CommandLine.run(tranModeCommand, TranMode.from(mode).name());
            menu();
        } catch (Exception e) {
            LogUtils.error(LOGGER, e);
            this.switchFastStandardTran();
        }
    }

    private void generateStorageFile() {
        CommandLine.run(genStorageFileCommand);
        menu();
    }

    private boolean isForceStandardProp() {
        VehicleStorage vehicleStorage = storageUtils.fetch();
        return vehicleStorage.isForceStandard();
    }

    private void fileExits() {
        Path filePath = Paths.get(StorageFileURL.getInstance(), STORAGE_FILE_NAME);
        try {
            File storageFile = new File(filePath.toString());

            if (storageFile.isFile()) {
                return;
            }
        } catch (Exception e) {
            LogUtils.error(LOGGER, e);
        }
        LogUtils.error(LOGGER, "Can not load file, please check the URL again...");
        LogUtils.error(LOGGER, "Restart the application and input correct URL...");
        menu();
    }
}
