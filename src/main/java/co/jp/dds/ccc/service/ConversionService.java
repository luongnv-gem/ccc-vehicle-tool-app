package co.jp.dds.ccc.service;

public interface ConversionService {
    void switchMode(Enum vehicleMode);
}
