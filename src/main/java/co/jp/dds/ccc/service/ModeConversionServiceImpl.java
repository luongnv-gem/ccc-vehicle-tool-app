package co.jp.dds.ccc.service;

import co.jp.dds.ccc.constant.VehicleMode;
import co.jp.dds.ccc.model.VehicleStorage;
import co.jp.dds.ccc.utils.StorageUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("modeConversion")
public class ModeConversionServiceImpl implements ConversionService {

    @Autowired
    private StorageUtils storageUtils;

    @Override
    public void switchMode(Enum vehicleMode) {
        VehicleStorage vehicleStorage = storageUtils.fetch();

        vehicleStorage.getPrivateMailbox().clearAllSlotIdentifier();
        vehicleStorage.getImmobilizerToken().clear();
        vehicleStorage.setPaired(false);
//        if (vehicleMode == VehicleMode.PAIRING && vehicleStorage.isPaired()) {
//            vehicleStorage.getPrivateMailbox().clearAllSlotIdentifier();
//            vehicleStorage.getImmobilizerToken().clear();
//            vehicleStorage.setPaired(false);
//        }
//
//        if (vehicleMode == VehicleMode.LOCK_UNLOCK ) {
//            vehicleStorage.setPaired(true);
//        }
        storageUtils.update(vehicleStorage);
    }

}
