package co.jp.dds.ccc.service;

import co.jp.dds.ccc.VehicleToolApp;
import co.jp.dds.ccc.model.StorageFileURL;
import co.jp.dds.ccc.model.VehicleStorage;
import co.jp.dds.ccc.utils.LogUtils;
import co.jp.dds.ccc.utils.PemFileUtils;
import org.bouncycastle.jce.interfaces.ECPublicKey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.cert.X509Certificate;
import java.security.interfaces.ECPrivateKey;
import java.util.List;

@Service
public class PrepareDataForVehicle {

	private static Logger LOGGER = LoggerFactory.getLogger(VehicleToolApp.class);

	private static final String STORAGE_FILE_NAME = "vehicle_storage.ccc";
	private static final String ROOT_PATH = StorageFileURL.getInstance();

	public static void run() {
		VehicleStorage storage = new VehicleStorage();
		try {
			storage.setVehicleSK(loadSK());
			storage.setVehiclePK(loadPK());
			storage.setVehiclePublicKeyCert(loadX509Cert());
			storage.setVehicleOEMCACert(loadVehicleOEMCA());
			List<byte[]> authorizedPKs = storage.getAuthorizedPKs();
			authorizedPKs.add(loadVehicleOemPK());
			storage.setAuthorizedPKs(authorizedPKs);
			write(storage);
			System.out.println("Generated storage file successfully ... ");
		} catch (Exception e) {
			LogUtils.error(LOGGER, "Failed to load file error from resources...");
		}		
	}

	private static VehicleStorage read() {
		Path filePath = Paths.get(ROOT_PATH, STORAGE_FILE_NAME);
		InputStream is = null;
		VehicleStorage vehicleStorage;
		ObjectInputStream ois = null;
		try {
			File storageFile = new File(filePath.toString());
			is = new FileInputStream(storageFile);
			ois = new ObjectInputStream(is);
			vehicleStorage = (VehicleStorage) ois.readObject();
		} catch (Exception e) {
			vehicleStorage = new VehicleStorage();
		} finally {
			try {
				if (ois != null) {
					ois.close();
				}
				if (is != null) {
					is.close();
				}
			} catch (IOException e) {
				LogUtils.error(LOGGER, "load file error...");
			}
		}
		return vehicleStorage;
	}
	
	private static void write(VehicleStorage vehicleStorage) {
		if (vehicleStorage == null) {
			return;
		}
		Path filePath = Paths.get(ROOT_PATH, STORAGE_FILE_NAME);
		try {
			File storageFile = new File(filePath.toString());
			File parent = storageFile.getParentFile();
			if(!parent.exists()) {
				parent.mkdir();
			}			
			FileOutputStream fileOut = new FileOutputStream(storageFile);
			ObjectOutputStream objectOut = new ObjectOutputStream(fileOut);
			objectOut.writeObject(vehicleStorage);
			objectOut.flush();
			objectOut.close();
		} catch (IOException e) {
			LogUtils.error(LOGGER, "Write file error...");
		}
	}
	
	private static byte[] loadSK() throws Exception {
		Path filePath = Paths.get(ROOT_PATH, "SK.pem");
		ECPrivateKey sk = (ECPrivateKey) PemFileUtils.generateECPrivateKey(filePath.toFile());
		return sk.getEncoded();
	}
	
	private static byte[] loadVehicleOemPK() throws Exception {
		Path filePath = Paths.get(ROOT_PATH, "vehicle_oem1_pk.pem");
		ECPublicKey pk = (ECPublicKey) PemFileUtils.generatePublicKey(filePath.toFile());
		return pk.getEncoded();
	}
	
	private static byte[] loadVehicleOEMCA() throws Exception {
		Path filePath = Paths.get(ROOT_PATH, "self-signed-VehicleOEMCA_1.com[J].cer");
		X509Certificate cert =  PemFileUtils.loadX509CertFromFile(filePath.toFile());
		return cert.getEncoded();
	}
	
	private static byte[] loadPK() throws Exception {
		Path filePath = Paths.get(ROOT_PATH, "PK.pem");
		ECPublicKey pk = (ECPublicKey) PemFileUtils.generatePublicKey(filePath.toFile());
		return pk.getEncoded();
	}
	
	private static byte[] loadX509Cert() throws Exception {
		Path filePath = Paths.get(ROOT_PATH, "vehicleOEMCA_1-signed-vehicle_1.com[K].cer");
		X509Certificate cert =  PemFileUtils.loadX509CertFromFile(filePath.toFile());
		return cert.getEncoded();
	}
}
