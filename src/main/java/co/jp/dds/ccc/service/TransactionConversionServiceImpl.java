package co.jp.dds.ccc.service;

import co.jp.dds.ccc.constant.TranMode;
import co.jp.dds.ccc.model.VehicleStorage;
import co.jp.dds.ccc.utils.StorageUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("transactionConversion")
public class TransactionConversionServiceImpl implements ConversionService {

    @Autowired
    private StorageUtils storageUtils;

    @Override
    public void switchMode(Enum tranMode) {
        VehicleStorage vehicleStorage = storageUtils.fetch();
        if (TranMode.FAST == (tranMode)) {
            vehicleStorage.setForceStandard(false);
        }

        if (TranMode.STANDARD == tranMode) {
            vehicleStorage.setForceStandard(true);
        }
        storageUtils.update(vehicleStorage);
    }
}
