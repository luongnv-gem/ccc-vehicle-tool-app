package co.jp.dds.ccc.command.mode;

import co.jp.dds.ccc.command.mode.subcommand.LockUnlock;
import co.jp.dds.ccc.command.mode.subcommand.Pairing;
import org.springframework.stereotype.Component;
import picocli.CommandLine;

@CommandLine.Command(
        subcommands = {
                Pairing.class,
                LockUnlock.class
        }
)
@Component
public class ModeConversionCommand implements Runnable {
    public static void main(String[] args) {
        CommandLine.run(new ModeConversionCommand(), args);
    }

    @Override
    public void run() {
        System.out.println("Switching Communication mode ... ");
    }
}
