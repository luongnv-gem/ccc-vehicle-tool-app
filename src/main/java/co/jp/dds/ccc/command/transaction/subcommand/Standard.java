package co.jp.dds.ccc.command.transaction.subcommand;

import co.jp.dds.ccc.constant.TranMode;
import co.jp.dds.ccc.model.SpringContext;
import co.jp.dds.ccc.service.ConversionService;
import org.springframework.stereotype.Component;
import picocli.CommandLine;


@CommandLine.Command(
        name = "STANDARD"
)
@Component
public class Standard implements Runnable{

    @Override
    public void run() {
        ConversionService transactionConversion = (ConversionService) SpringContext.getApplicationContext().getBean("transactionConversion");
        transactionConversion.switchMode(TranMode.STANDARD);
        System.out.println("STANDARD TRANSACTION mode on .....");
    }
}

