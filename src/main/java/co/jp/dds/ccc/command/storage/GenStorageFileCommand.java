package co.jp.dds.ccc.command.storage;

import co.jp.dds.ccc.service.PrepareDataForVehicle;
import org.springframework.stereotype.Component;
import picocli.CommandLine;

@CommandLine.Command
@Component
public class GenStorageFileCommand implements Runnable {

    public static void main(String[] args) {
        CommandLine.run(new GenStorageFileCommand(), args);
    }

    @Override
    public void run() {
        System.out.println("Preparing Data for Vehicle ... ");
        PrepareDataForVehicle.run();
    }
}
