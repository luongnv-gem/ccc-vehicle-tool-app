package co.jp.dds.ccc.command.mode.subcommand;

import co.jp.dds.ccc.constant.VehicleMode;
import co.jp.dds.ccc.model.SpringContext;
import co.jp.dds.ccc.service.ConversionService;
import co.jp.dds.ccc.service.ModeConversionServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import picocli.CommandLine;

import javax.annotation.Resource;

@CommandLine.Command(
        name = "PAIRING"
)
@Component
public class Pairing implements Runnable{

    @Override
    public void run() {
        ConversionService modeConversion = (ConversionService) SpringContext.getApplicationContext().getBean("modeConversion");
        modeConversion.switchMode(VehicleMode.PAIRING);
        System.out.println("PAIRING MODE ON .....");
    }
}
