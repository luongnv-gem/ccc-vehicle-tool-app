package co.jp.dds.ccc.command.transaction;

import co.jp.dds.ccc.command.transaction.subcommand.Fast;
import co.jp.dds.ccc.command.transaction.subcommand.Standard;
import org.springframework.stereotype.Component;
import picocli.CommandLine;

@CommandLine.Command(
        subcommands = {
                Fast.class,
                Standard.class
        }
)
@Component
public class TransactionModeCommand implements Runnable {
    public static void main(String[] args) {
        CommandLine.run(new TransactionModeCommand(), args);
    }

    @Override
    public void run() {
        System.out.println("Switching transaction mode ... ");
    }
}
