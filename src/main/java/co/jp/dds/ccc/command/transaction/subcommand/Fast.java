package co.jp.dds.ccc.command.transaction.subcommand;

import co.jp.dds.ccc.constant.TranMode;
import co.jp.dds.ccc.model.SpringContext;
import co.jp.dds.ccc.service.ConversionService;
import org.springframework.stereotype.Component;
import picocli.CommandLine;

@CommandLine.Command(
        name = "FAST"
)
@Component
public class Fast implements Runnable{

    @Override
    public void run() {
        ConversionService transactionConversion = (ConversionService) SpringContext.getApplicationContext().getBean("transactionConversion");
        transactionConversion.switchMode(TranMode.FAST);
        System.out.println("FAST TRANSACTION mode on .....");
    }
}

