This describes how to use Vehicle Tool App to manage environment variables and generate storage file

## First, Input Storage File URL

This is working directory url of **the existed storage file** OR **location for file generation**

---
After enter storage file url, a menu gonna be displayed

## 1, Switch Pairing Mode/ Lock-Unlock mode

You only can switch to Pairing mode from Lock/Unlock mode

---

## 2, Switch Fast transaction / Standard Transaction mode.

The transaction mode can be interchanged between Fast and Standard

---

## 3, Generate storage file

Storage file will be generated to the directory that you have entered at very first based on resources (PK, SK, CA,... ) at the same directory